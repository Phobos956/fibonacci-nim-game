/**
 * This is the Fibonacci Nim Game. The rules are:
 * The game starts with 3 heaps of 9 tokens in each.
 * The first player, at first can choose to take 1 or 2 tokens from only 1 heap.
 * Then it's the second player's turn. And they can choose to take from 1 to double the maximum tokens taken so far.
 * Players are allowed to take tokens from 1 heap only on each turn.
 * The winner is the player who takes the last remaining token.
 *
 * @author Ventsislav Yordanov 
 */

import java.util.Scanner;

public class FibonacciNim
{
	public static void main(String[] args)
	{
		Scanner in = new Scanner(System.in);
		
		int heap1 = 9;
		int heap2 = 9;
		int heap3 = 9;
		
		int heap = 0; // Will represent which heap the player picked.
		int token = 0;// Will represent how many tokens the player chose .
		
		int maxToken = 1; /* Used to represent the highest amount of tokens taken so far.
		And to start off with, we will assign it to its lowest possible value, 1. */
		
		int turn = 1; /* Tracks at which phase the game is. If turn is odd, then it is Player 1's turn.
		Respectively if turn is an even number, then it is Player 2's turn. */
		
		/* The following block is for the first phase of the game only. In other words
		 * it is only used when Player 1 makes their first move. */
		System.out.println("Heap 1 " + heap1);
		System.out.println("Heap 2 " + heap2);
		System.out.println("Heap 3 " + heap3);
		System.out.println("Player 1's turn.");
		System.out.print("Choose a heap (1-3): ");
		
		/* The following block will check if the player enters integer to choose the heap.
		 * It will also check if such heap exists and if not, will ask for a new input. */
		boolean invalidHeap = true;
		while(invalidHeap == true)
		{
			while(!in.hasNextInt())
			{
				System.out.print("Please choose a heap (1-3): ");
				in.nextLine();
			}
			heap = in.nextInt();
			in.nextLine();
			if(heap >= 1 && heap <= 3)
			{
				invalidHeap = false;
			}
			else
			{
				System.out.println("Heap " + heap + " does not exist.");
				System.out.print("Please choose a heap (1-3): ");
			}
		}
		
		System.out.println("The  number of tokens you may take is between 1 and " + "2");
		System.out.print("How many tokens do you want to take? ");
		
		/* The following block will check if the player enters integer for the amount of
		 * tokens they want to take. It will also check if the amount is within the permitted range and
		 * if not, will ask for a new input. */
		boolean invalidToken = true;
		while(invalidToken == true)
		{
			while(!in.hasNextInt())
			{
				System.out.print("Please enter the number of tokens you want to take: ");
				in.nextLine();
			}
			token = in.nextInt();
			in.nextLine();
			if(token >= 1 && token <= 2)
			{
				invalidToken = false;
			}
			else
			{
				System.out.println("You cannot take " + token + " tokens.");
				System.out.println("The  number of tokens you may take is between 1 and " + "2");
				System.out.print("How many tokens do you want to take? ");
			}
		}
		
		if(heap == 1)
		{
			heap1 = heap1 - token;
		}
		else if (heap == 2)
		{
			heap2 = heap2 - token;
		}
		else if (heap == 3)
		{
			heap3 = heap3 - token;
		}
		//Check if the last entered token is greater than maxToken. If it is, then maxToken takes its value.
		if(token > maxToken)
		{
			maxToken = token;
		}
		
		turn++;
		
		/* After the first phase has ended, the following loop is entered.
		 * The same loop will be executed until eventually no tokens remain. */
		while(heap1 > 0 || heap2 > 0 || heap3 > 0)
		{
			System.out.println("Heap 1 " + heap1);
			System.out.println("Heap 2 " + heap2);
			System.out.println("Heap 3 " + heap3);
			if(turn % 2 == 1) // Check if turn is an even or odd number, so that we can tell if it is P1 or P2's turn.
			{
				System.out.println("Player 1's turn.");
			}
			else 
			{
				System.out.println("Player 2's turn.");
			}
			
			turn++;
			
			// The player chooses a heap. The input will be checked just like in the first phase of the game.
			System.out.print("Choose a heap (1-3): ");
			invalidHeap = true;
			while(invalidHeap == true)
			{
				while(!in.hasNextInt())
				{
					System.out.print("Please choose a heap (1-3): ");
					in.nextLine();
				}
				heap = in.nextInt();
				in.nextLine();
				if(heap >= 1 && heap <= 3)
				{
					invalidHeap = false;
				}
				else
				{
					System.out.println("Heap " + heap + " does not exist.");
					System.out.print("Please choose a heap (1-3): ");
				}
			}
			
			int canTake = 0; //Will be used to track the maximum tokens a player can take.
			
			if(maxToken == 1) /* This "if" is only used for the cases when no one has ever chosen to
			take more than 1 token. */
			{
				System.out.println("The  number of tokens you may take is between 1 and " + maxToken * 2);
				canTake = maxToken * 2;
			}
			else
			{
				if(heap == 1 && (maxToken * 2) > heap1) /* Prevents displaying that you can take more tokens than
				there are in the heap. */
				{
					System.out.println("The  number of tokens you may take is between 1 and " + heap1);
					canTake = heap1;
				}
				else if(heap == 2 && (maxToken * 2) > heap2)
				{
					System.out.println("The  number of tokens you may take is between 1 and " + heap2);
					canTake = heap2;
				}
				else if(heap == 3 && (maxToken * 2) > heap3 )
				{
					System.out.println("The  number of tokens you may take is between 1 and " + heap3);
					canTake = heap3;
				}
				else
				{
					System.out.println("The  number of tokens you may take is between 1 and " + maxToken * 2);
					canTake = maxToken * 2;
				}				
			}
			
			// The player chooses amount of tokens to take. The input will be checked just like in the first phase.
			System.out.print("How many tokens do you want to take? ");
			
			invalidToken = true;
			while(invalidToken == true)
			{
				while(!in.hasNextInt())
				{
					System.out.print("Please enter the number of tokens you want to take: ");
					in.nextLine();
				}
				token = in.nextInt();
				in.nextLine();
				if(token >= 1 && token <= canTake)
				{
					invalidToken = false;
				}
				else
				{
					System.out.println("You cannot take " + token + " tokens.");
					System.out.println("The number of tokens you may take is between 1 and " + canTake);
					System.out.print("How many tokens do you want to take? ");
				}
			}
			
			if(heap == 1)
			{
				heap1 = heap1 - token;
			}
			else if (heap == 2)
			{
				heap2 = heap2 - token;
			}
			else if (heap == 3)
			{
				heap3 = heap3 - token;
			}
			if(token > maxToken)
			{
				maxToken = token;
			}
		}
		
		/* The winner is declared depending on whether the phase(turn), at which the game ended
		 * was an even or odd number. */
		if(turn % 2 == 0)
		{
			System.out.println("Player 1 wins");
		}
		else if(turn % 2 == 1)
		{
			System.out.println("Player 2 wins");
		}
	}
}